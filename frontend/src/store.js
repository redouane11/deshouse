import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { productListReducer, 
         productDetailsReducer, 
         productDeleteReducer,
         productCreateReducer,
         productUpdateReducer,
         productReviewCreateReducer
        } from './reducers/productReducers'
import { cartReducer } from './reducers/cartReducers'
import { nightModeReducer } from './reducers/nightModeReducer'
import { userLoginReducer,
         userRegisterReducer,
         userDetailsReducer,
         userUpdateProfileReducer,
        userListReducer, 
        userDeleteReducer,
        userUpdateReducer
    } from './reducers/userReducers'

         import { orderCreateReducer, 
                  orderDetailsReducer,
                  orderPayReducer,
                  orderListMyReducer,
                  orderListReducer,
                  orderDeliverReducer
        } from './reducers/orderReducers'
const reducer = combineReducers({
    productList: productListReducer,
    productDetails: productDetailsReducer,
    productDelete: productDeleteReducer,
    productCreate: productCreateReducer,
    productUpdate: productUpdateReducer,
    producteReviewCreate: productReviewCreateReducer,
    cart: cartReducer,
    night: nightModeReducer,
    userLogin: userLoginReducer,
    userRegister: userRegisterReducer,
    userDetails: userDetailsReducer,
    userUpdateProfile: userUpdateProfileReducer,
    userList: userListReducer,
    userDelete: userDeleteReducer,
    userUpdate: userUpdateReducer,
    orderCreate: orderCreateReducer,
    orderDetails: orderDetailsReducer,
    orderPay: orderPayReducer,
    orderListMy: orderListMyReducer,
    orderList: orderListReducer,
    orderDeliver: orderDeliverReducer
    
})
const cartItemsFromLocalStorage = localStorage.getItem('cartItems')
    ? JSON.parse(localStorage.getItem('cartItems'))
    : []

    const shippinAddressFromStorage = localStorage.getItem('shippingAddress')
    ? JSON.parse(localStorage.getItem('shippingAddress'))
    : {}
    const paymentMethodFromStorage = localStorage.getItem('shippingAddress')
    ? JSON.parse(localStorage.getItem('paymentMethod'))
    : ''

const userInfoFromLocalStorage = localStorage.getItem('userInfo')
    ? JSON.parse(localStorage.getItem('userInfo'))
    : null

const nightModeFromLocalStorage = localStorage.getItem('nightMode')
    ? JSON.parse(localStorage.getItem('nightMode'))
    : false
const initiaState = {
    cart: {cartItems:cartItemsFromLocalStorage,
           numberOfItems: cartItemsFromLocalStorage.length,
           shippingAddress: shippinAddressFromStorage,
           paymentMethod: paymentMethodFromStorage
        },
        
    night:{
        nightMode: nightModeFromLocalStorage
    },
    userLogin: {
        userInfo: userInfoFromLocalStorage
    }
     
}

const middleware = [thunk]

const store = createStore(
    reducer,
    initiaState,
    composeWithDevTools(applyMiddleware(...middleware)))


export default store