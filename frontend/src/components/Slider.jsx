import React from 'react'
import { useState }from "react";
import { Carousel, Image} from 'react-bootstrap'

 const Slider = ({sliderData}) => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  console.log('sliderData = ' ,sliderData[0].img)
  
  return (
      <div>
    <Carousel  activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item interval={100000}>
        <Image 
          
          src={`../..public/${sliderData[0].img}`}
          className="d-block w-100"
          rounded alt={sliderData[0].name}
         fluid
         
        />
        <Carousel.Caption>
          <h3>{sliderData[0].subtitle}</h3>
        </Carousel.Caption>
        <Carousel.Caption>
          <p>{sliderData[0].description}</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={100000}>
        <Image

          className="d-block w-100"
          src="https://picsum.photos/800/400?text=Second "
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>{sliderData[1].subtitle}</h3>
          <p>{sliderData[1].Description}</p>
        </Carousel.Caption>
      </Carousel.Item >
      <Carousel.Item interval={100000}>
        <Image
          className="d-block w-100"
          src="https://picsum.photos/800/400?text=Second"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>{sliderData[1].subtitle}</h3>
          <p>
            {sliderData[1].description}
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>
  );
}



export default Slider