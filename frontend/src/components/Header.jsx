    import React, {useState }from 'react'
    import { useDispatch, useSelector } from 'react-redux'
    import { LinkContainer } from 'react-router-bootstrap'
    import { Navbar, Container, Nav, Form, NavDropdown } from 'react-bootstrap'
    import ProfileScreen from '../screens/ProfileScreen'

    import { enableNightMode } from '../actions/enableNightMode'
import { logout } from '../actions/userActions'
    
    const Header = ({history}) => {

        const [modalShow, setModalShow] = React.useState(false);
        
        const [nightMode, setNightMode] = useState(false)
        const dispatch = useDispatch()
       
        const userLogin = useSelector(state => state.userLogin)
        const { userInfo } = userLogin

        const {cartItems} = useSelector(state => state.cart)

       // console.log('cartItems from Header', cartItems)

        const onchangeHandler =() =>{
            setNightMode(!nightMode)
            dispatch(enableNightMode(nightMode))
        }

        const logoutHandler = () => {
            dispatch(logout())
        }


       
        return (
            <header>
                <Navbar  bg="light" expand="lg"  collapseOnSelect>
                    <Container >
                    <Form > 
                        
                        <Form.Check 
                            type="switch"
                            id="custom-switch"
                            label={nightMode? 'Day': 'Night'}
                            style={{ color: "red" }} 
                            onChange ={onchangeHandler}
                            

                        
                        />
                        
                        </Form>

                        <LinkContainer to='/' >
                          <Navbar.Brand className="navText">
                          <h4 className="navText">DesHome</h4> 
                          </Navbar.Brand>
                        </LinkContainer>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
            <LinkContainer to='/cart'>
                <Nav.Link>
                    <i className={cartItems.length? "item-added-cart icon fas fa-shopping-cart primary"
                    :"icon fas fa-shopping-cart primary"}>
                    
                        </i>
                        <h4 className="navText">Cart</h4> 
                </Nav.Link>
          </LinkContainer>
          {userInfo ? (
             <NavDropdown title={userInfo.name} id="username">
                 <NavDropdown.Item onClick={() => setModalShow(true)}>Profile
                  </NavDropdown.Item>
                  <ProfileScreen
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
                  <NavDropdown.Item href='/myorders'>My orders
                    
                  </NavDropdown.Item>
                  <NavDropdown.Item onClick={logoutHandler}>Logout
                  </NavDropdown.Item>

             </NavDropdown>
          ) : <LinkContainer to='/login'>
          <Nav.Link> 
          <i className="icon fas fa-user">
              </i>
              <h4 className="navText">Sign In
              </h4>
              </Nav.Link>
              </LinkContainer>}
          {userInfo && userInfo.isAdmin && (
                <NavDropdown title='Admin' id="adminmenu">
                     <LinkContainer to='/admin/userlist'>
                        <NavDropdown.Item >
                            Users
                       </NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to='/admin/productlist'>
                         <NavDropdown.Item >
                             Products
                         </NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to='/admin/orderlist'>
                          <NavDropdown.Item >
                             Orders
                        </NavDropdown.Item>
                     </LinkContainer>

            </NavDropdown> 
          )}
        </Nav>
        
      </Navbar.Collapse>
      </Container>
    </Navbar>
            </header>
        )
    }

    export default Header
