import React, { useState, useEffect }from 'react'
import Toast from 'react-bootstrap/Toast'
import {Row, Col} from 'react-bootstrap/'
const ToastComponent = (props) => {
    const [show, setShow] = useState(true);
console.log("toooast")
    return (
        
        <Row>
        <Col xs={6} aria-live="polite" aria-atomic="true" style={{position: "relative", minHeight: "200px"}}>
          <Toast 

                 style={{position: "absolute", top: "8px", right: "16px"}}
                 onClose={() => setShow(false)}
                 show={show}
                 delay={3000} autohide>
            <Toast.Header>
            <i className="far fa-check-circle" style={{color:"green"}}></i>
              <strong>{props.info}</strong>
            </Toast.Header>
            <Toast.Body>Woohoo, Product removed by {props.message}!</Toast.Body>
          </Toast>
        </Col>
        
      </Row>
    )
}

export default ToastComponent
