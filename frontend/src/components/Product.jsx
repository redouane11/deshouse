import React from 'react'
import { useSelector } from 'react-redux'
import { Card} from 'react-bootstrap'
import Rating from './Rating'

const Product = ({product}) => {
    return (
        
            <Card className='my-3 p-3 rounded card-product'>
                <a href={`/product/${product._id}` } className="card-img-top">
                    <Card.Img  src={product.image} variant='top'></Card.Img>
                </a>
                <Card.Body  >
                    <a href={`/product/${Product._id}`}>
                        <Card.Title as='div'>
                            
                            <strong>{product.name}</strong>
                        
                           
                        </Card.Title>
                    </a>

                    <Card.Text as='div'>
                        <Rating value={product.rating} text={`${product.numReviews} reviews`} />
                    </Card.Text>
                    <Card.Text as='h3'>${product.price}</Card.Text>
      
      <Card.Text>
        This card has supporting text below as a natural lead-in to additional
        content.{' '}
      </Card.Text>
    </Card.Body>
            </Card>
        
    )
}

Product.propTypes = {

}

export default Product
