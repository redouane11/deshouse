import { ENABLE_NIGHT_MODE } from '../constants/nightMode'

export const enableNightMode = (night) =>  (dispatch, getState) => {
         
        dispatch({
            type: ENABLE_NIGHT_MODE,
            payload: night
        })
        localStorage.setItem('nightMode', JSON.stringify(getState().night.nightMode))

}