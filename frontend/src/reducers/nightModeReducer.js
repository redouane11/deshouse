import { ENABLE_NIGHT_MODE } from '../constants/nightMode'

export const nightModeReducer = (state = { nightMode: false}, action ) => {
    
    switch (action.type) {
        case ENABLE_NIGHT_MODE:
            const mode = action.payload
            
            
                    return {
                        ...state,
                        nightMode:mode
                    }
        default:
            return state
    }
}
