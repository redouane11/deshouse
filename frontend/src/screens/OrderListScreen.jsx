import React, { useState, useEffect }from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import ConfirmModal from '../components/ConfirmModal'
//import MyAccordionComponent from  '../components/MyAccordionComponent'
import { listOrders} from '../actions/orderActions'

const OrderListScreen = ({history, match}) => {

    const dispatch = useDispatch()

    const orderList =  useSelector(state =>state.orderList)
    const { loading, error, orders} = orderList

    const userLogin =  useSelector(state =>state.userLogin)
    const { userInfo} = userLogin

   
    useEffect(() => {
       
        if(userInfo && userInfo.isAdmin) {
            dispatch(listOrders())
           
        } else{
            history.push('/login')
        }
       
    }, [dispatch, history, userInfo])

    

    
    return (
        <>
                  

        <Row className="align-items-center">
            <Col>
                <h1>Orders</h1>
            </Col>
            
        </Row>
           <Table striped bordered hover responsive className='table-sm text-center'>
               <thead>
             
                  <tr>
                    <th  >ID#</th>
                    <th>NAME</th>
                    <th  >DATE#</th>
                    <th>TOTAL PRICE</th>
                    <th  >PAID</th>
                    <th>DELIVERED</th>


                  </tr>
               </thead>
               <tbody>
                   {orders && orders.map(order => (
                       <tr key={order._id}>
                          <td>{order._id}</td>
                          <td>{order.user && order.user.name}</td>
                          <td>{order.createdAt.substring(0,10)}</td>
                          <td>${order.totalPrice}</td>
                          <td>{order.isPaid ? (
                              order.paidAt.substring(0, 10)): (
                                  <i className="fas fa-times" style={{color: 'red'}}></i>
                              )
                          }</td>
                           <td>{order.isDelivered ? (
                              order.deliveredAt.substring(0, 10)): (
                                  <i className="fas fa-times" style={{color: 'red'}}></i>
                              )
                          }</td>
                            <td>
                             <LinkContainer to={`/order/${order._id}`}>
                                    <Button variant='light'  className='btn-sm'>
                                        Details
                                    </Button>
                                    </LinkContainer>
                             </td>     

                        </tr>

                       
                   ))}
               </tbody>
           
           
           
           </Table>
        </>
    )
}

export default OrderListScreen
