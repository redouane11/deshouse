import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col} from 'react-bootstrap'

import sliderData from '../helpers/sliderData'
import Product from '../components/Product'
import Slider from '../components/Slider'
import Message from '../components/Message'
import Loader from '../components/Loader'
//import axios from 'axios'
import { listProducts } from '../actions/productActions'

const HomeScreen = () => {
    //const [products, setProducts] = useState([])
    
    /* Using async await

    useEffect( () => {
        const getProducts = async () => {
            const products = await axios('/api/products')
            setProducts(products.data)
        }
   
        getProducts()

  
    }, [])
    */

   /* Using async then

    const fetchProducts = () => {
        axios.get('api/products')
        .then( response => {
            const products = response.data
            setProducts(products)
        })
    }
    useEffect(() => {
        fetchProducts()


    }, [])
   */
   
   /*
   Using REDUX
   */

    const dispatch = useDispatch()
    const productList = useSelector(state => state.productList)
    const {loading, error, products} = productList

    useEffect(() => {
            dispatch(listProducts())
    }, [dispatch])

    
    return (
        <>
           <div >
               
               <Slider sliderData={sliderData} />
            </div>   
           <h1>
               Latest Products 
           </h1> 
           {loading ? (
               <Loader />
               ) : error ? (
                <Message variant='danger'>{error}</Message>
               ) : (
           <Row > 
               
               {products.map(product =>(
                
                   <Col key={product._id} sm={12} md={6} lg={4} xl={3} >
                       <Product product={product}  />
                   
                   </Col>
                 
               ))}
           </Row>
               )}
               
        </>
    )
}

export default HomeScreen;
