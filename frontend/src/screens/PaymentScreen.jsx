import React, { useState }from 'react'
import { FormControl, InputGroup,Form, Button, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'

//import { login } from '../actions/userActions'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { savePaymentMethod } from '../actions/cartActions'


const PaymentScreen = ({history}) => {

    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    if(!shippingAddress) {
        history.push('/shipping')
    }

    const [paymentMethod, setPaymentMethod] = useState('PayPal')
    


    const dispatch = useDispatch()


    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(savePaymentMethod(paymentMethod))
        history.push('/placeorder')
    }

    return (
        <FormContainer>
            <CheckoutSteps step1 step2 step3 />
            <h1>Payment Method</h1>
            <Form onSubmit={submitHandler}>
              <Form.Group>
                  <Form.Label as='legend'>
                      Select Method
                  </Form.Label>
            

              <Col>
              <InputGroup  className="mb-3">
    <Form.Check  type='radio'
                    label='PayPal '
                    id='PayPal' 
                    name='paymentMethode' 
                    value='PayPal ' 
                    checked
                    onChange={(e)=>setPaymentMethod(e.target.value)
                    }>
                        

                </Form.Check> 
                <span> </span>
                <InputGroup.Prepend sm="10">
                     <i  className="icon-paypal fab fa-cc-paypal"></i>
    </InputGroup.Prepend>
 </InputGroup>

 <InputGroup size="sm" className="mb-3">
    <Form.Check type='radio'
                    label='Credit Card '
                    id='creditCard' 
                    name='paymentMethode' 
                    value='CreditCard ' 
                    checked
                    onChange={(e)=>setPaymentMethod(e.target.value)
                    }>
                        

                </Form.Check> 
                <span> </span>
                <InputGroup.Prepend>
                <i className="far fa-credit-card icon-credit"></i>    </InputGroup.Prepend>
 </InputGroup>

               
                             </Col>

                             </Form.Group >
               <Button type='submit' variant='primary'>
                   Continue
               </Button>
               </Form> 

        </FormContainer>
    )
}

export default PaymentScreen
