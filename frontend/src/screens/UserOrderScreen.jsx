
    import React, { useState, useEffect }from 'react'
    import { Link } from 'react-router-dom'
    import { Table, Button } from 'react-bootstrap'
    import { LinkContainer } from 'react-router-bootstrap'
    import { useDispatch, useSelector } from 'react-redux'
    import Message from '../components/Message'
    import Loader from '../components/Loader'
    //import { login } from '../actions/userActions'
    import { getUserDetails, UpdateUserProfile } from '../actions/userActions'
    import { listMyOrders } from '../actions/orderActions'


const UserOrderScreen = () => {

        const dispatch =  useDispatch()
    
        const orderListMy  = useSelector(state => state.orderListMy)
        const { loading, error, orders } = orderListMy
       // const {a, paymentResult, ...b}  = orders
         

        
        useEffect(() => {
            dispatch(listMyOrders())
            
            
        }, [dispatch])

   return (
       <>
        <div>
            My Orders
        </div>
        {loading ? <Loader/> : error ? <Message variant='danger'>{error}</Message> : (
            <Table responsive striped bordered hover  className='table-sm'>
                 <thead>
                   <tr>
                        <th >Order#</th>
                        <th>Order Date</th>
                        <th>Total Amount</th>
                        <th>Paid</th>
                        <th>Delivered</th>
                        <th></th>
                  </tr>
                </thead>
                <tbody>
                  {orders && orders.map(order => (
                      <tr key={order._id} >
                         <td > {order._id}</td>
                         <td>{order.createdAt.substring(0,10)}</td>
                         <td>{order.totalPrice}</td>
                         <td>{order.isPaid ? order.paidAt.substring(0,10) : (
                           <i className="fas fa-times" style={{color: 'red'}}></i>
                              )}
                         </td>
                        <td className='table-dark'>{order.isDelivered ? order.deliveredAt.substring(0,10) : (
                           <i className="fas fa-times" style={{color: 'red'}}></i>
                              )}
                        </td>           
                       <td>
                           <LinkContainer to={`/orders/&{order_id}`}>
                                <Button className='btn-sm' variant='light'>Show order</Button>
                           </LinkContainer>
                       </td>
                      </tr>
                  ))}
      
     
  </tbody>
</Table>
        ) }
        
       </> 
    )
}

export default UserOrderScreen
