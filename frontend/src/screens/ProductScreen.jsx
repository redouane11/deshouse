import React , { useState, useEffect }from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'


import { Row, Col, Image,
    Card,  ListGroup, Button, ListGroupItem, Form } from 'react-bootstrap'
import Rating from '../components/Rating'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { detailsProduct, CreateProductReview } from '../actions/productActions'
import { PRODUCT_CREATE_REVIEW_RESET } from '../constants/productConstants'
//import axios from 'axios'


const ProductScreen = (props) => {
    //const [ product, setProduct] = useState({})
    const [ qty, setQty] = useState(1)
    const [ rating, setRating] = useState(0)
    const [ comment, setComment] = useState('')



    /*
    Without Using REDUX


    useEffect( () =>{
    const getProduct = async () => {
        const product = await axios(`/api/products/${props.match.params.id}`)
        console.log(product)
        setProduct(product.data)
    }       
    getProduct()
}, [])
*/


/*
Without Using REDUX

useEffect( () => {
    const fetchProduct = () => {
        axios.get(`/api/products/${props.match.params.id}`)
        .then( response => {
            const {data} = response
            setProduct(data)
        } )
    }
    fetchProduct()

}, [props.match])
*/

/* 
    Using REDUX
*/
    const dispatch = useDispatch()

    const productDetails = useSelector(state => state.productDetails)
    const {loading, error, product} = productDetails

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin


    const producteReviewCreate = useSelector(state => state.producteReviewCreate)
    const {success: reviewCreateSuccess, loading: reviewCreateLoading, error: reviewCreateError, message: reviewCreateMessage} = producteReviewCreate

    useEffect(() => {
        
        dispatch(detailsProduct(props.match.params.id))
    
        
    }, [dispatch, props.match])

    const addToCartHandler = () => {
        props.history.push(`/cart/${props.match.params.id}?qty=${qty}`)

    }

    return (
        <>
            <Link to="/" className="btn btn-primary my-3">
                <i className="fas fa-arrow-left"></i>
            </Link>
            {loading ? (
               <Loader />
               ) : error ? (
                <Message variant='danger'>{error}</Message>
               ) : (
             <>      
            <Row>
                <Col  md={6}>
                    <Image src={product.image} rounded alt={product.name} fluid className="card-img-top">
                        
                    </Image>
                </Col>
                <Col md={3}>
                    <ListGroup  variant="flush"  >
                        <ListGroup.Item className="list-item">
                            <h3>{product.name}</h3>
                            Description: <br/>
                                {product.description} 
                        </ListGroup.Item>
                        <ListGroup.Item className="list-item">
                            <Rating value={product.rating}
                                text={`${product.numReviews} reviews`}
                                color="orange"
                            />
                        </ListGroup.Item>
                        <ListGroup.Item className="list-item">
                            Price: ${product.price}
                        </ListGroup.Item>       
                    </ListGroup>
                </Col>
                <Col md={3}>
                <ListGroup variant="flush" className="list-item">
                  <Card className="list-item"> 
                    <ListGroup.Item className="list-item" >
                        <Row >
                            <Col>
                                Price:
                            </Col>
                            <Col>
                                <strong>${product.price}</strong>
                            </Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item className="list-item" >
                        <Row >
                            <Col>
                                Availability:
                            </Col>
                            <Col>
                                {
                                product.countInStock> 0 ? 
                                <div className="product-available">
                                    In Stock
                                </div>: 
                                <div className="product-not-available">
                                    Out Of Stock</div>
                                    }
                            </Col>
                        </Row>
                    </ListGroup.Item>
                    {product.countInStock>0 && (
                      <ListGroupItem>
                        <Row>
                            <Col>Quatity</Col>
                            <Col>
                                <Form.Control as='select'
                                value={qty}
                                onChange={e=>setQty(e.target.value)}>
                                    {
                                      [...Array(product.countInStock).keys()].map((x) => (
                                          <option key={x+1} value={x+1}>
                                              {x+1}
                                          </option>
                                      ))  
                                    }
                                
                                </Form.Control>
                            </Col>
                        </Row>
                    </ListGroupItem>
                    )}
                    <ListGroup.Item className="list-item">
                    <Button type='button'  
                        onClick={addToCartHandler}
                        className='btn btn-primary  btn-block' 
                        disabled={product.countInStock===0} >
                        Add to Cart</Button>

                    </ListGroup.Item>
                  </Card>
                </ListGroup>
                
                
                </Col>

            </Row>
            <Row>
                <Col md={6} >
                    <h2 >Reviews</h2>
                    {product.reviews.length === 0 && <Message variant='warning' >No Reviews</Message>}
                    <ListGroup variat='flush'>
                        {product.reviews.map(review =>(
                            <ListGroup.Item key={review._id}>
                                <strong>{review.name}</strong>
                                <Rating value={review.rating}/> 
                                <p>{review.createdAt.substring(0,10)}</p> 
                                <p>{review.comment}</p>
                             </ListGroup.Item>
                               
                        ))}
                    </ListGroup>
                </Col>
            </Row>
            </>
               )}

        </>
    )
}

export default ProductScreen
