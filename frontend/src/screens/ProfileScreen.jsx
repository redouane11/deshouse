import React, { useState, useEffect }from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col, Modal } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
//import { login } from '../actions/userActions'
import { getUserDetails, UpdateUserProfile } from '../actions/userActions'


const ProfileScreen = ({show, onHide, location, history}) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState(null)

    
    const dispatch = useDispatch()


    const userDetails  = useSelector(state => state.userDetails)
    const { loading, error, user } = userDetails

    const userLogin  = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const userUpdateProfile  = useSelector(state => state.userUpdateProfile)
    const { success } = userUpdateProfile


    useEffect(() =>{
        if(!userInfo) {
           history.push('/login')
        } else {
            if(!user){
                dispatch(getUserDetails('profile'))
            } else {
                setName(user.name)
                setEmail(user.email)
            } 
        }
    },[history, userInfo, dispatch, user])

    const submitHandler = (e) =>{
        e.preventDefault()
        if(password !== confirmPassword) {
            setMessage('Passwords do not match')
        } else {
           dispatch(UpdateUserProfile({id: user._id, name, email, password}))
           console.log('ok')
        }
    }

    return <Modal
        
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="Edit Profile"
        centered
        >
             <Modal.Header>
            
        <Modal.Title id="EditProfile">
          Modal heading
        </Modal.Title>
      </Modal.Header>
       <Modal.Body>
    <Row>
            <Col md={3}>
            <h2>User Profile</h2>
        {message && <Message variant='danger'>{message}</Message>}
        {success && <Message variant='success'>Profile Updated</Message>}
    
     {error && <Message variant='danger'>
                {error}
               </Message>}
     {loading && <Loader />}

            <Form onSubmit={submitHandler}>
                 <Form.Group controlId="name">
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                       type="name" 
                       placeholder="Enter name" 
                       value={name} 
                       onChange={(e) => setName(e.target.value)}     
                       />
                 </Form.Group>
                 <Form.Group controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                       type="email" 
                       placeholder="Enter email" 
                       value={email} 
                       onChange={(e) => setEmail(e.target.value)}     
                       />
                    <Form.Text className="text-muted">
                       We'll never share your email with anyone else.
                    </Form.Text>
                 </Form.Group>

                 <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                       type="password" 
                       placeholder="Password" 
                       value={password} 
                       onChange={(e) => setPassword(e.target.value)}     
                       />                  
                 </Form.Group>
                 <Form.Group controlId="confirmPassword">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control 
                       type="password" 
                       placeholder="Confirm Password" 
                       value={confirmPassword} 
                       onChange={(e) => setConfirmPassword(e.target.value)}     
                       />                  
                 </Form.Group>
                 <Button variant="primary" type="submit">
                     update
                </Button>
            </Form>
            
            </Col>
            <Col md={9}>
                <h2>
                    My orders
                </h2>
            </Col>
        </Row>
        </Modal.Body>
    <Modal.Footer>
    <Button onClick={onHide}>Close</Button>
  </Modal.Footer>
</Modal>
        
    
}

export default ProfileScreen
