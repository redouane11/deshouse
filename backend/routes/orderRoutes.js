import express from 'express'
import {isAdmin, protect} from '../middleware/authMiddleware.js'

import { 
    addOrderItems,
    getOrderById,
    updateOrderPaid,
    getMyorders,
    getOorders,
    updateOrderToDelivered
} from '../controllers/orderController.js'

const router = express.Router()


router.route('/').post(protect, addOrderItems).get(protect, isAdmin, getOorders)
router.route('/myorders').get(protect, getMyorders)
router.route('/:id').get(protect, getOrderById)
router.route('/:id/pay').put(protect, updateOrderPaid)
router.route('/:id/deliver').put(protect,isAdmin, updateOrderToDelivered)







export default router