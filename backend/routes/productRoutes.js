import express from 'express'

import { getProductById,
         getProducts, 
         deleteProductById, 
         updateProduct,
         createProduct,
         createProductReviews
    } from '../controllers/productController.js'
import {protect, isAdmin} from '../middleware/authMiddleware.js'

const router = express.Router()


// router.get('/', getProducts)
router.route('/').get(getProducts).post(protect, isAdmin, createProduct)
//router.get(:/:id', getProductById)
router.route('/:id').get(getProductById).delete(protect, isAdmin, deleteProductById).
put(protect, isAdmin, updateProduct)
router.route('/:id/reviews').post(protect, createProductReviews)



export default router