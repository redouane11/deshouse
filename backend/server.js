
import express, { json } from 'express'
import dotenv from 'dotenv'
import colors from 'colors'
import morgan from 'morgan'
import path from 'path'
import connectDB from './config/db.js'
import { notFound, errorHandler } from './middleware/errorMiddlewate.js'
import productRoutes from './routes/productRoutes.js'
import uploadRoutes from './routes/uploadRoutes.js'
import userRoutes from './routes/userRoutes.js'
import orderRoutes from './routes/orderRoutes.js'

 

dotenv.config()

connectDB()

const app = express()
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'))
}
// It parses incoming requests with JSON payloads 
// and is based on body-parser
app.use(express.json());
app.get('/', (req, res) => {
    res.send('API IS RUNNING!')
})

app.use('/api/upload', orderRoutes)
app.use('/api/products', productRoutes)
app.use('/api/users', userRoutes)
app.use('/api/orders', orderRoutes)

app.get('/api/config/paypal', (req,res) => {
    res.send(process.env.PAYPAL_CLIENT_ID)
})
/*    /uploads folder by default is not accessible by default
      by the browser, to make it accessible the folder have to be static 
*/
const __dirname = path.resolve()

app.use('/uploads', express.static(path.join(__dirname, '/uploads')))

app.use(notFound)

app.use(errorHandler)

const port = process.env.PORT || 5000
app.listen(port, console.log(`Server runninig in ${process.env.NODE_ENV} mode on port ${port}`.blue.bold))