import mongoose from 'mongoose'
import asyncHandler from 'express-async-handler'
import Product from '../models/productModel.js'


/*
    @desc   Fetch all products
    @route  GET/api/products
    @access Public 

*/
const getProducts = asyncHandler(async (req, res) =>{
    const products = await Product.find({})

    res.json(products)

})

/*
    @desc   Fetch sing products
    @route  GET/api/products/:id
    @access Public 

*/
const getProductById = asyncHandler(async (req, res) =>{
    const productId = mongoose.Types.ObjectId(req.params.id)
    const product = await Product.findById(productId)

    if(!product){
        res.status(404)
           
        throw new Error('Product not Found')
      
      
        }
    else {
        
      res.json(product)
    }
})


/*
    @desc   Deletea product
    @route  DELETE/api/products/:id
    @access Private/Admin 

*/
const deleteProductById = asyncHandler(async (req, res) =>{
    const product = await Product.findById(req.params.id)

    if(product){
        await product.remove()
        res.status(200).json({message: "Product removed"})
           
      
      
        }
    else {
        res.status(400)
        throw new Error('Product not Found')
 
    }
})

/*
    @desc   Create a product
    @route  POST/api/products
    @access Private/Admin 

*/
const createProduct = asyncHandler(async (req, res) =>{
    const product = new Product({
        name:'Sample name',
        price: 0,
        user: req.user._id,
        image:'/images/sample.jpg',
        brand:'Sample barad',
        category:'Sample category',
        countInStock: 0,
        numReviews:0,
        description:'Sample descriptin',
    })
    
    const createdProduct = await product.save()

        res.status(201).json(createdProduct)
})

/*
    @desc   Update a product
    @route  PUT/api/products/:id
    @access Private/Admin 

*/
const updateProduct = asyncHandler(async (req, res) =>{
    const {
        name,
        price,
        image,
        brand,
        category,
        countInStock ,
        description,
    } = req.body
    
    const product = await Product.findById(req.params.id)
    if(product) {

        product.name = name
        product.price = price
        product.image = image
        product.brand = brand
        product.category = category
        product.countInStock = countInStock
        product.description = description

        const updatedProduct = await product.save()

        res.status(201).json(updatedProduct)

    } else {
        res.status(404)
        throw new Error('Product not found')
    }
})


/*
    @desc   Create new review
    @route  POST/api/products/:id/reviews
    @access Private 

*/
const createProductReviews = asyncHandler(async (req, res) =>{
    const { rating, comment } = req.body
    
    const product = await Product.findById(req.params.id)


    if(product) { 
        const alreadyReviwed = product.reviews.find(r => r.user.toString() === req.user._id.toString())

        if(alreadyReviwed){
            res.status(400)
            throw new Error(`Product already reviewes by ${req.user.name}`)
        }
        const review = {
            name: req.user.name,
            rating: Number(rating),
            comment,
            user: req.user._id
          }
         product.reviews.push(review)
        product.numReviews = product.reviews.length
        console.log(" reviews:    ", product.reviews)

        product.rating = product.reviews.reduce(( acc, item ) => item.rating + acc, 0)/product.reviews.length

         await product.save()

        res.status(201).json({message: 'Review added'})

    } else {
        res.status(404)
        throw new Error('Product not found')
    }
})



export {getProducts, getProductById, deleteProductById, createProduct, updateProduct, createProductReviews }