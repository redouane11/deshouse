
const products = [
    {
        name: 'Sofa 1',
        description: 'Best Sofa 1, Best Sofa 1, Best Sofa, Best Sofa 1.',
        image: '/images/sofa1.jpg',
        brand: 'IKEA',
        category: 'living room',
        price: 199.99,
        countInStock: 3,
        rating: 4.5,
        numReviews: 4

},
{
    name: 'Sofa 2',
    description: 'Best Sofa 2, Best Sofa 2, Best Sofa 2, Best Sofa 2.',
    image: '/images/sofa2.jpg',
    brand: 'IKEA',
    category: 'living room',
    price: 189.99,
    countInStock: 3,
    rating: 4,
    numReviews: 3

},

{

    name: 'Sofa 3',
    description: 'Best Sofa 3, Best Sofa 3, Best Sofa 3, Best Sofa 3.',
    image: '/images/sofa3.jpg',
    brand: 'Strucktube',
    category: 'living room',
    price: 299.99,
    countInStock: 5,
    rating: 3.5,
    numReviews: 0

},

{
    name: 'kitchen 1',
    description: 'Best kitchen 1, Best kitchen 1, Best kitchen 1, Best kitchen 1, Best kitchen 1',
    image: '/images/kitchen1.jpg',
    brand: 'IKEA',
    category: 'kitchen',
    price: 549.99,
    countInStock: 10,
    rating: 5,
    numReviews: 9

},

{
    name: 'kitchen 2',
    description: 'Best kitchen 2, Best kitchen 2, Best kitchen 2, Best kitchen 2, Best kitchen 2',
    image: '/images/kitchen2.jpg',
    brand: 'Strucktube',
    category: 'kitchen',
    price: 649.99,
    countInStock: 8,
    rating: 3,
    numReviews: 8

},

{

    name: 'kitchen 3',
    description: 'Best kitchen 3, Best kitchen 3, Best kitchen 3, Best kitchen 3, Best kitchen 3',
    image: '/images/kitchen3.jpg',
    brand: 'IKEA',
    category: 'kitchen',
    price: 349.99,
    countInStock: 6,
    rating: 2.5,
    numReviews: 3

},

{
    name: 'bed 1',
    description: 'Best bed 1, Best bed 1, Best bed 1, Best bed 1, Best bed 1',
    image: '/images/bed1.jpg',
    brand: 'IKEA',
    category: 'bed',
    price: 299.99,
    countInStock: 10,
    rating: 4,
    numReviews: 12

},

{
    name: 'bed 2',
    description: 'Best bed 2, Best bed 2, Best bed 2, Best bed 2, Best bed 2',
    image: '/images/bed2.jpg',
    brand: 'IKEA',
    category: 'bed',
    price: 199.99,
    countInStock: 0,
    rating: 5,
    numReviews: 12

},

{
    name: 'bed 3',
    description: 'Best bed 3, Best bed 3, Best bed 3, Best bed 3, Best bed 3',
    image: '/images/bed3.jpg',
    brand: 'Strucktube',
    category: 'bed',
    price: 99.99,
    countInStock: 1,
    rating: 5,
    numReviews: 20

},

{
    name: 'kids 1',
    description: 'Best kids 1 , Best kids 1, Best kids 1, Best kids 1, Best kids 1',
    image: '/images/kids1.jpg',
    brand: 'IKEA',
    category: 'kids',
    price: 99.99,
    countInStock: 0,
    rating: 5,
    numReviews: 20

},

{
    name: 'kids 2',
    description: 'Best kids 2 , Best kids 2, Best kids 2, Best kids 2, Best kids 2',
    image: '/images/kids2.jpg',
    brand: 'Strucktube',
    category: 'kids',
    price: 59.99,
    countInStock: 2,
    rating: 5,
    numReviews: 2

},

{
    name: 'kids 3',
    description: 'Best kids 3, Best kids 3, Best kids 3, Best kids 3, Best kids 3',
    image: '/images/kids3.jpg',
    brand: 'Strucktube',
    category: 'kids',
    price: 159.99,
    countInStock: 3,
    rating: 4.5,
    numReviews: 12

},

{
    name: 'desk 1',
    description: 'Best desk 1, Best desk 1, Best desk 1, Best desk 1, Best desk 1',
    image: '/images/desk1.jpg',
    brand: 'Strucktube',
    category: 'desk',
    price: 49.99,
    countInStock: 1,
    rating: 5,
    numReviews: 3

},
{
    name: 'desk 2',
    description: 'Best desk 2, Best desk 2, Best desk 2, Best desk 2, Best desk 2',
    image: '/images/desk2.jpg',
    brand: 'IKEA',
    category: 'desk',
    price: 59.99,
    countInStock: 0,
    rating: 3,
    numReviews: 10

},

{
    name: 'desk 3',
    description: 'Best desk 3, Best desk 3, Best desk 3, Best desk 3, Best desk 3',
    image: '/images/desk3.jpg',
    brand: 'IKEA',
    category: 'desk',
    price: 89.99,
    countInStock: 5,
    rating: 4,
    numReviews: 10

},

{
    name: 'yard 1',
    description: 'Best yard 1,Best yard 1,Best yard 1,Best yard 1,Best yard 1',
    image: '/images/yard1.jpg',
    brand: 'IKEA',
    category: 'yard',
    price: 129.99,
    countInStock: 2,
    rating: 4,
    numReviews: 10

},

{
    
    name: 'yard 2',
    description: 'Best yard 2,Best yard 2,Best yard 2,Best yard 2,Best yard 2',
    image: '/images/yard2.jpg',
    brand: 'IKEA',
    category: 'yard',
    price: 139.99,
    countInStock: 1,
    rating: 3,
    numReviews: 10

},

{
    name: 'yard 3',
    description: 'Best yard 3,Best yard 3,Best yard 3,Best yard 3,Best yard 3',
    image: '/images/yard3.jpg',
    brand: 'IKEA',
    category: 'yard',
    price: 119.99,
    countInStock: 0,
    rating: 5,
    numReviews: 12

}

]
export default products