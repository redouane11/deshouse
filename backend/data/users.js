import bcrypt from 'bcryptjs'

const users = [
    {
        name: 'Redouane Adriouch',
        email: 'admin@deshome.com',
        password: bcrypt.hashSync('123456', 10),
        isAdmine: true

    },
    {
        name: 'Ilyas Adriouch',
        email: 'ilyas@deshome.com',
        password: bcrypt.hashSync('123456', 10)

    },
    {
        name: 'Aimrane Adriouch',
        email: 'aimrane@deshome.com',
        password: bcrypt.hashSync('123456', 10)

    }
]


export default users